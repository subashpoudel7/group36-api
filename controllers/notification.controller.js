const NotificationModel = require('./../models/notification.model');
const router = require('express').Router();

router.get('/', function (req, res, next) {
    // get all notifications
    var condition = {
        user: req.user._id
    };
    NotificationModel
        .find(condition)
        .sort({
            _id: -1
        })
        .limit(10)
        .populate('user_id', {
            username: 1
        })
        .exec(function (err, results) {
            if (err) {
                return next(err);
            }
            res.json(results);
        })
})



router.get('/mark_as_read/:id', function (req, res, next) {
    // 
    NotificationModel.update({ _id: req.params.id }, {
        $set: { seen: true }
    }, function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done)
    })
})

router.get('/mark_all_as_read', function (req, res, next) {
    // 
    NotificationModel.update({
        user: req.user._id
    }, {
        $set: { seen: true }
    }, {
        multi: true
    }, function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done)
    })
})
router.get('/:id', function (req, res, next) {
    NotificationModel.findById(req.params.id, function (err, noti) {
        if (err) {
            return next(err);
        }
        res.json(noti);
    })
})

router.delete('/:id', function (req, res, next) {
    // 
    NotificationModel.findByIdAndRemove(req.params.id, function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done)
    })
})


module.exports = router;
