const ProudctQuery = require('./product.query');
const removeFile = require('./../../helpers/remove_file')
// sub route
// aplication logic

function post(req, res, next) {
    if (req.fileTypeErr) {
        return next({
            msg: "Invalid File Format",
            status: 400
        })
    }

    console.log('fies >>', req.files)
    const data = req.body;
    data.vendor = req.user._id;
    if (req.files && req.files.length) {
        data.images = req.files.map(function (file) {
            return file.filename;
        })
    }
    ProudctQuery
        .insert(data)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}

function get(req, res, next) {
    var condition = {};
    if (req.user.role !== 1) {
        condition.vendor = req.user._id
    }
    ProudctQuery
        .find(condition, req.query) // own find (not of monoogse)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })

}
function update(req, res, next) {
    const data = req.body;
    if (req.fileTypeErr) {
        return next({
            msg: "Invalid File Format",
            status: 400
        })
    }

    if (data.vendor && typeof (data.vendor) === 'object') {
        data.vendor = data.vendor._id;
    }

    if (req.files && req.files.length) {
        data.newImages = req.files.map(function (file) {
            return file.filename;
        })
    }

    // note images are in string and old data re corrupted so remove existing images from request
    delete data.images;


    const filesToRemove = data.filesToRemove
        .split(',')
        .map(img => {
            return img.split('images/')[1]
        });

    data.filesToRemove = filesToRemove;
    ProudctQuery
        .update(req.params.id, data)
        .then(function (response) {
            // on success and if we have req.files.length
            // remove old images
            // remove existing images if we have req.filetoremove
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })

}
function remove(req, res, next) {
    ProudctQuery
        .remove(req.params.id)
        .then(function (response) {
            // remove existing images from server
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}
function search(req, res, next) {
    console.log('req.body >>', req.body)
    const searchCondition = {};
    if (req.body.category)
        searchCondition.category = req.body.category
    if (req.body.name)
        searchCondition.name = req.body.name
    if (req.body.brand)
        searchCondition.brand = req.body.brand
    if (req.body.color)
        searchCondition.color = req.body.color
    if (req.body.minPrice)
        searchCondition.price = {
            $gte: req.body.minPrice
        }
    if (req.body.maxPrice)
        searchCondition.price = {
            $lte: req.body.maxPrice
        }
    if (req.body.minPrice && req.body.maxPrice)
        searchCondition.price = {
            $lte: req.body.maxPrice,
            $gte: req.body.minPrice
        }
    if (req.body.fromDate && req.body.toDate) {
        const fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0)
        const toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999);
        searchCondition.createdAt = {
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
        }
    }
    if (req.body._id) {
        searchCondition._id = req.body._id
    }
    // tags
    // $in, $all
    // $all it apply condition that all provided items should match
    // $in .> any item can match
    if (req.body.tags) {
        let tags = typeof (req.body.tags) === 'string'
            ? req.body.tags.split(',')
            : req.body.tags
        searchCondition.tags = {
            $in: tags
        }
    }


    console.log('search condition >', searchCondition)
    ProudctQuery
        .find(searchCondition)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })

}
function addReview(req, res, next) {
    if (!(req.body.reviewMessage && req.body.reviewPoint)) {
        return next({
            msg: 'missing required fields',
            status: 400
        })
    }
    const productId = req.params.product_id;
    const reviewData = {
        message: req.body.reviewMessage,
        point: req.body.reviewPoint,
        user: req.user._id,
    }
    // add reviewer 
    ProudctQuery
        .addReview(productId, reviewData)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}

function getById(req, res, next) {

    const condition = {
        _id: req.params.id
    }
    ProudctQuery
        .find(condition)
        .then(function (response) {
            res.json(response[0])
        })
        .catch(function (err) {
            next(err);
        })
}

module.exports = {
    post: post,
    get,
    getById,
    search,
    update,
    remove,
    addReview
}

