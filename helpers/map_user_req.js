module.exports = function (user, userData) {
    if (userData.firstName)
        user.firstName = userData.firstName;
    if (userData.lastName)
        user.lastName = userData.lastName;
    if (userData.email)
        user.email = userData.email;
    if (userData.role)
        user.role = userData.role;
    if (userData.dob)
        user.dob = userData.dob;
    if (userData.gender)
        user.gender = userData.gender
    if (userData.username)
        user.username = userData.username;
    if (userData.password)
        user.password = userData.password;
    if (userData.country)
        user.country = userData.country;
    if (userData.status)
        user.status = userData.status;

    // deal with object
    if (!user.contactDetails)
        user.contactDetails = {};
    if (userData.mobileNumber)
        user.contactDetails.mobile = userData.mobileNumber;
    if (userData.homeNumber)
        user.contactDetails.home = userData.homeNumber;
    if (userData.alternateNumber)
        user.contactDetails.alternateNumber = userData.alternateNumber;
    if (!user.address)
        user.address = {};
    if (userData.tempAddress)
        user.address.temporaryAddress = userData.tempAddress.split(',');
    if (userData.permanentAddress)
        user.address.permanentAddress = userData.permanentAddress;
    if (userData.image)
        user.image = userData.image;


    return user;
}
